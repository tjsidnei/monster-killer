new Vue({
  el: '#app',
  data: {
    running: false,
    playerLife: 100,
    monsterLife: 100,
    logs: []
  },
  computed: {
    hasResult () {
      const self = this
      return self.playerLife === 0 || self.monsterLife === 0
    }
  },
  methods: {
    startGame () {
      const self = this
      self.running = true
      self.playerLife = 100
      self.monsterLife = 100
      self.logs = []
    },
    attack (special) {
      const self = this
      self.hurt('monsterLife', 5, 10, special, 'Jogador', 'Monstro', 'player')
      if (self.monsterLife > 0) self.hurt('playerLife', 7, 12, false, 'Monstro', 'Jogador', 'monster')
    },
    hurt (prop, min, max, special, source, target, cls) {
      const self = this
      const plus = special ? self.getRandom(3, 5) : 0
      const hurt = self.getRandom(min + plus, max + plus)
      self[prop] = Math.max(self[prop] - hurt, 0)
      self.registerLog(`${source} atingiu ${target} com ${hurt}`, cls)
    },
    healAndHurt () {
      const self = this
      self.heal(10, 15)
      self.hurt('playerLife', 7, 12, false, 'Monstro', 'Jogador', 'monster')
    },
    heal (min, max) {
      const self = this
      const heal = self.getRandom(min, max)
      self.playerLife = Math.min(self.playerLife + heal, 100)
      self.registerLog(`Jogador ganhou força de ${heal}`, 'player')
    },
    getRandom (min, max) {
      let value = Math.random() * (max - min) + min
      return Math.round(value)
    },
    registerLog (text, cls) {
      const self = this
      self.logs.unshift({ text, cls })
    }
  },
  watch: {
    hasResult (value) {
      if (value) this.running = false
    }
  }
})